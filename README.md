# JSON API
This is hosted on Heroku at [here](https://halloffame-server.herokuapp.com/) with CI/CD on Gitlab. in case you can not use Heroku server or need to change the code you can clone the repo from [here](https://gitlab.com/arashkay/halloffameserver/tree/master) and run locally. 
There is also a [test.html](https://halloffame-server.herokuapp.com/test.html) file in the original project to show how you can use the API in JS which you can download from gitlab repo.

## How to work with this API?
Both `/fames` and `/fames/:id` have authorisation which can be bypassed in case it is not required in your task.

If in your tasks you have to implement login before calling the above APIs we have provided `/login` & `/logout` APIs.

### Want to skip Authorization?
But in case your task has no login/logout you can skip all Authorizations by passing `?guest=true` at the end of API calls in the URL (e.g. `/fames?guest=true` this will bypass auth for the fames endpoint).

## Endpoints
Here you can see all available endpoints. You can extend these endpoints by cloning it and running it locally.

### Health
Check health of the system.
```text
GET /health
```

### Login
Login API which is needed before calling other endpoints with **test** as username/password. `username=test` & `password=test`. You will get a JWT token in response Header called `authorization`.
Which you can use in other API calls by passing in the header as `Authorization: 'Bearer JWTToken'`.
```text
POST /login
```

> **Note:** If you use POSTMAN to test this API, you need to send those parameters as form-data.
> If you still cannot figure out how to login, read the [sample source code](https://gitlab.com/arashkay/halloffameserver/blob/master/test.html) 

### Logout
Logout API which can be called after login API to clear the session.
```text
POST /logout
```

### List Fames
Getting fames list. This API has pagination `?page=value` with value equal to 0 or 1. Each page has 8 items to show.
```text
GET /fames?page=0|1
```

### Get a fame
Getting a fame by ID. You can find IDs in `/fames` API.
```text
GET /fames/:id
```
