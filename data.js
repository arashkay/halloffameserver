const fames = [

  { id: '54rt5rrd', name: 'Kit Harington', dob: '23/03/1968', image: 'https://upload.wikimedia.org/wikipedia/commons/3/32/Kit_harrington_by_sachyn_mital_%28cropped_2%29.jpg' },
  { id: 'vgyh54db', name: 'Sophie Turner', dob: '07/07/1968', image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Sophie_Turner_%2840553145603%29_%28cropped%29.jpg/440px-Sophie_Turner_%2840553145603%29_%28cropped%29.jpg' },
  { id: 'vfgdxh7y', name: 'Maisie Williams', dob: '18/08/1968', image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Maisie_Williams_by_Gage_Skidmore_3.jpg/440px-Maisie_Williams_by_Gage_Skidmore_3.jpg' },
  { id: 'gfhrt66e', name: 'Peter Dinklage', dob: '20/04/1968', image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/Peter_Dinklage_by_Gage_Skidmore.jpg/440px-Peter_Dinklage_by_Gage_Skidmore.jpg' },
  { id: 'j44gsryh', name: 'Lena Headey', dob: '13/09/1968', image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Lena_Headey_%2847086135862%29_%28cropped%29.jpg/440px-Lena_Headey_%2847086135862%29_%28cropped%29.jpg' },
  { id: 'cgfh324g', name: 'Nikolaj Coster-Waldau', dob: '28/06/1968', image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Nikolaj_Coster-Waldau_by_Gage_Skidmore_2.jpg/440px-Nikolaj_Coster-Waldau_by_Gage_Skidmore_2.jpg' },
  { id: 'sdg46gsv', name: 'Gwendoline Christie', dob: '21/02/1968', image: 'https://upload.wikimedia.org/wikipedia/commons/6/6e/Gwendoline_christie_by_sachyn_mital_%28cropped2%29.jpg' },
  { id: 'sgg42efs', name: 'Nathalie Emmanuel', dob: '15/01/1968', image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Nathalie_Emmanuel_by_Gage_Skidmore.jpg/440px-Nathalie_Emmanuel_by_Gage_Skidmore.jpg' },
  { id: 'aush17dh', name: 'Robert De Niro', dob: '17/05/1943', image: 'https://m.media-amazon.com/images/M/MV5BMjAwNDU3MzcyOV5BMl5BanBnXkFtZTcwMjc0MTIxMw@@._V1_UY209_CR9,0,140,209_AL_.jpg' },
  { id: 'ksj7621', name: 'Jack Nicholson', dob: '12/01/1952', image: 'https://m.media-amazon.com/images/M/MV5BMTQ3OTY0ODk0M15BMl5BanBnXkFtZTYwNzE4Njc4._V1_UY209_CR5,0,140,209_AL_.jpg' },
  { id: 'jdh8280', name: 'Tom Hanks', dob: '12/01/1958', image: 'https://m.media-amazon.com/images/M/MV5BMTQ2MjMwNDA3Nl5BMl5BanBnXkFtZTcwMTA2NDY3NQ@@._V1_UY209_CR2,0,140,209_AL_.jpg' },
  { id: 'ksj97dh', name: 'Audrey Hepburn', dob: '04/07/1929', image: 'https://m.media-amazon.com/images/M/MV5BMTM4MTY3NTQyMF5BMl5BanBnXkFtZTYwMTk2MzQ2._V1_UX140_CR0,0,140,209_AL_.jpg' },
  { id: '34km8wc', name: 'Halle Berry', dob: '14/01/1960', image: 'https://m.media-amazon.com/images/M/MV5BMjIxNzc5MDAzOV5BMl5BanBnXkFtZTcwMDUxMjMxMw@@._V1_UY209_CR7,0,140,209_AL_.jpg' },
  { id: 'kd98h2b', name: 'Julia Roberts', dob: '22/03/1955', image: 'https://m.media-amazon.com/images/M/MV5BMTQzNjU3MDczN15BMl5BanBnXkFtZTYwNzY2Njc4._V1_UY209_CR0,0,140,209_AL_.jpg' },
  { id: 'fka93hkd', name: 'Al Pacino', dob: '25/07/1940', image: 'https://m.media-amazon.com/images/M/MV5BMTQzMzg1ODAyNl5BMl5BanBnXkFtZTYwMjAxODQ1._V1_UX140_CR0,0,140,209_AL_.jpg' },
  { id: '6gw3gcsn', name: 'Will Smith', dob: '25/09/1968', image: 'https://m.media-amazon.com/images/M/MV5BNTczMzk1MjU1MV5BMl5BanBnXkFtZTcwNDk2MzAyMg@@._V1_UY209_CR2,0,140,209_AL_.jpg' }
]

export default fames