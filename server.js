import Koa from 'koa'
import Router from 'koa-router'
import Parser from 'koa-body'
import jwt from 'koa-jwt'
import jsonwebtoken from 'jsonwebtoken'
import cors from '@koa/cors'
import showdown from 'showdown'
import fs from 'fs'
import _ from 'lodash'
import fames from './data'


const PORT = process.env.PORT || 3000
const SECRET = 'shared-secret'
const app = new Koa()
const router = new Router()
const converter = new showdown.Converter()

const isGuest = (ctx) => (ctx.request.query && ctx.request.query.guest === 'true')
const isNotLoggedIn = (ctx) => {
  return (!ctx.state.jwtdata && !isGuest(ctx)) || (ctx.state.jwtdata && ctx.state.jwtdata.auth !== true) // session is not created or user is not guest
}
const getFames = (page) => {
  const chunks = _.chunk(fames, 8)
  return page > 1 ? [] : chunks[page]
}
  
router.post('/login', (ctx, next) => {
  const success = (ctx.request.body.username === 'test'
    && ctx.request.body.password === 'test' )
  const token = jsonwebtoken.sign({ auth: success }, SECRET)
  ctx.set('Authorization', token)
  ctx.body = {
    data: {
      success
    },
    error: null
  }
})

router.post('/logout', (ctx, next) => {
  ctx.session = null
  ctx.body = {
    data: { success: true },
    error: null
  }
})

router.get('/fames', (ctx, next) => {
  const list = getFames(ctx.request.query.page || 0)
  ctx.body = {
    data: {
      list: list,
      size: list.length
    },
    error: null
  }
})

router.get('/fames/:id', (ctx, next) => {
  ctx.body = {
    data: _.find(fames, (item) => item.id === ctx.params.id),
    error: null
  }
})

router.get('/health', (ctx, next) => {
  ctx.body = { data: { success: true, message: 'All APIs are healthy' }, error: null }
})

router.get('/', (ctx, next) => {
  const template = fs.readFileSync('template.html', 'utf8')
  ctx.body = template.replace('{content}', converter.makeHtml(fs.readFileSync('README.md', 'utf8')))
})

router.get('/test.html', (ctx, next) => {
  ctx.body = fs.readFileSync('test.html', 'utf8')
})

app
  .use(cors({
    exposeHeaders: 'Authorization',
    credentials: true
  }))
  .use(jwt({ secret: SECRET, passthrough: true, key: 'jwtdata' }))
  .use(Parser({ multipart: true }))
  .use(async (ctx, next) => {
    if (ctx.request.url !== '/health' && ctx.request.url !== '/test.html' && ctx.request.url !== '/' && ctx.request.url !== '/login' && isNotLoggedIn(ctx)) {
      ctx.status = 401
      ctx.body = { error: 'not logged in' }
    } else {
      return next()
    }
  })
  .use(router.routes())
  .use(router.allowedMethods())

app.listen(PORT, () => console.log(`server started on port ${PORT}`))